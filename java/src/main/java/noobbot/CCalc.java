package noobbot;

public class CCalc
{
    private CRace Race;

    public CCalc(CRace Race) {
        this.Race=Race;
    }
    public CPiece NextPiece(CPiece Piece) {
        return Race.Pieces.get((Piece.Index+1)%Race.Pieces.size());
    }
    public CPiece NextPiece(CPiece Piece,int n) {
        return Race.Pieces.get((Piece.Index+n)%Race.Pieces.size());
    }
    public double LaneLength(CPiece Piece,int Lane) {
        if (Piece.Type==EPieceType.Straight) {
            return Piece.Length;
        } else {
            if (Piece.Angle>0) {
                return Math.abs(2*3.141592653589*(Piece.Radius-Race.Lanes.get(Lane).DistanceFromCenter)*(Piece.Angle/360.0));
            } else {
                return Math.abs(2*3.141592653589*(Piece.Radius+Race.Lanes.get(Lane).DistanceFromCenter)*(Piece.Angle/360.0));
            }
        }
    }
    public double MaxVelocity(CPiece Piece,int Lane) {
        if (Piece.Type==EPieceType.Curve) {
            if (Piece.Angle>0) {
                return Math.sqrt(Main.TrackCoefficient*(Piece.Radius-Race.Lanes.get(Lane).DistanceFromCenter));
            } else {
                return Math.sqrt(Main.TrackCoefficient*(Piece.Radius+Race.Lanes.get(Lane).DistanceFromCenter));
            }
        } else {
            return 1000.0;
        }
    }
    public double MaxVelocity(CPiece Piece,int Lane,int LookAhead) {
        int p;
        double MinMaxVelocity=1000.0;
        double MaxVelocity;
        CPiece StartPiece=Piece;
        for (p=0;p<LookAhead;p++) {
            MaxVelocity=MaxVelocity(Piece,Lane);
            if (MinMaxVelocity>MaxVelocity) MinMaxVelocity=MaxVelocity;
            Piece=NextPiece(Piece);
        }
        return MinMaxVelocity;
    }
    public double LaneLength(int Piece,int Lane) {
        return LaneLength(Race.Pieces.get(Piece),Lane);
    }
}
