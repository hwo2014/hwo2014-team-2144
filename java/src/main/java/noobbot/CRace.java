package noobbot;

import com.google.gson.stream.JsonReader;
import java.util.ArrayList;
import java.io.*;

enum EPieceType {
    Straight,
    Curve
}

class CPiece {
    int Index;
    // Visualization attributes
    double X1,Y1,X2,Y2;
    double InitAngle,EndAngle;
    // Data attributes
    EPieceType Type;
    double Length;
    double Radius;
    double Angle;
    boolean Switch;
    public CPiece(JsonReader TrackData,int Index) throws IOException {
        this.Index=Index;
        boolean HasLength=false,HasRadius=false;
        TrackData.beginObject();
        while (TrackData.hasNext()) {
            String name=TrackData.nextName();
            if (name.equals("length")) {
                this.Length=TrackData.nextDouble();
                HasLength=true;
            } else if (name.equals("radius")) {
                this.Radius=TrackData.nextDouble();
                HasRadius=true;
            } else if (name.equals("angle")) {
                this.Angle=TrackData.nextDouble();
            } else if (name.equals("switch")) {
                this.Switch=TrackData.nextBoolean();
            }
        }
        TrackData.endObject();
        if (HasRadius && !HasLength) {Type=EPieceType.Curve;Length=2*3.141592653589*Radius*(Angle/360.0);} else Type=EPieceType.Straight;
    }
}

class CLane {
    double DistanceFromCenter;
    int Index;

    public CLane(JsonReader TrackData) throws IOException {
        TrackData.beginObject();
        while (TrackData.hasNext()) {
            String name=TrackData.nextName();
            if (name.equals("distanceFromCenter")) {
                this.DistanceFromCenter=TrackData.nextDouble();
            } else if (name.equals("index")) {
                this.Index=(int)TrackData.nextDouble();
            }
        }
        TrackData.endObject();
    }
}

public class CRace
{
    String ID;
    String Name;
    ArrayList<CPiece> Pieces;
    ArrayList<CLane> Lanes;
    ArrayList Cars;
    double StartX,StartY;
    double StartAngle;
    int Laps=1000;
    int DurationMs;
    int MaxLapTimeMs;
    boolean QuickRace;
    int FinishPiece;
    boolean Qualifier=false;
    
    public CRace(JsonReader TrackData) throws IOException {
        int TrackIndex=0;
        TrackData.beginObject();
        while (TrackData.hasNext()) {
            String name=TrackData.nextName();
            if (name.equals("race")) {
                TrackData.beginObject();
                while (TrackData.hasNext()) {
                    name=TrackData.nextName();
                    if (name.equals("track")) {
                        TrackData.beginObject();
                        while (TrackData.hasNext()) {
                            name=TrackData.nextName();
                            if (name.equals("id")) {
                                this.ID=TrackData.nextString();
                            } else if (name.equals("name")) {
                                this.Name=TrackData.nextString();
                            } else if (name.equals("pieces")) {
                                Pieces=new ArrayList<CPiece>();
                                TrackData.beginArray();
                                while (TrackData.hasNext()) {
                                    CPiece NewPiece;
                                    NewPiece=new CPiece(TrackData,TrackIndex++);
                                    this.Pieces.add(NewPiece);
                                }
                                TrackData.endArray();
                            } else if (name.equals("lanes")) {
                                Lanes=new ArrayList<CLane>();
                                TrackData.beginArray();
                                while (TrackData.hasNext()) {
                                    CLane NewLane;
                                    NewLane=new CLane(TrackData);
                                    this.Lanes.add(NewLane);
                                }
                                TrackData.endArray();
                            } else if (name.equals("startingPoint")) {
                                TrackData.beginObject();
                                name=TrackData.nextName();
                                TrackData.beginObject();
                                name=TrackData.nextName();
                                StartX=TrackData.nextDouble();
                                name=TrackData.nextName();
                                StartY=TrackData.nextDouble();
                                TrackData.endObject();
                                name=TrackData.nextName();
                                StartAngle=TrackData.nextDouble();
                                TrackData.endObject();
                            }
                        }
                        TrackData.endObject();
                    } else if (name.equals("cars")) {
                        TrackData.skipValue();
                    } else if (name.equals("raceSession")) {
                        TrackData.beginObject();
                        while (TrackData.hasNext()) {
                            name=TrackData.nextName();
                            if (name.equals("laps")) {
                                Laps=TrackData.nextInt();
                            } else if (name.equals("maxLapTimeMs")) {
                                MaxLapTimeMs=TrackData.nextInt();
                            } else if (name.equals("quickRace")) {
                                QuickRace=TrackData.nextBoolean();
                            } else if (name.equals("durationMs")) {
                                DurationMs=TrackData.nextInt();
                                Qualifier=true;
                            }
                        }
                        TrackData.endObject();
                    }
                }
                TrackData.endObject();
            }
        }
        TrackData.endObject();
    }
}
