package noobbot;  

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;

import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;
import java.io.StringReader;

public class Main {
    public static String MyColor="red";
    public static int GameTick=0;
    public static double TrackCoefficient=0.5;
    public static boolean Crashed=false;
    public static int Lap=0;
    
    public static boolean TurboAvailable=false;
    public static double TurboDurationMilliseconds;
    public static double TurboDurationTicks;
    public static double TurboFactor;
    
/*
        String host = "testserver.helloworldopen.com";
        int port = 8091;
        String botName = "Keldar";
        String botKey = "VSsSuEkxTDEoiw";
*/    
/*    public static void TrackFinland() throws IOException {
        OurMain("keimola");
    }
    public static void TrackGermany() throws IOException {
        OurMain("germany");
    }
    public static void TrackUSA() throws IOException {
        OurMain("usa");
    }
    public static void TrackFrance() throws IOException {
        OurMain("france");
    }
    public static void OurMain(String trackName) throws IOException {
        //senna.helloworldopen.com
        //hakkinen.helloworldopen.com
        String host = "testserver.helloworldopen.com";
        int port = 8091;
        String botName = "Petka";
        String botKey = "VSsSuEkxTDEoiw";
        if (trackName==null) trackName="keimola";
        

        System.out.println("Connecting to " + host + ":" + port + " as " + botName + "/" + botKey);
        final Socket socket;
        try {
            socket = new Socket(host, port);
        } catch (IOException E) {
            System.out.println("Cannot connect to server!");
            //E.printStackTrace();
            return;
        };
        final PrintWriter writer = new PrintWriter(new OutputStreamWriter(socket.getOutputStream(), "utf-8"));

        final BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream(), "utf-8"));
        SendMsg join;
        join=new CreateRace(trackName,null,botName, botKey);
        new Main(reader, writer, join);
    }
*/    
    public static void main(String... args) throws IOException {
        String host = args[0];
        int port = Integer.parseInt(args[1]);
        String botName = args[2];
        String botKey = args[3];

        System.out.println("Connecting to " + host + ":" + port + " as " + botName + "/" + botKey);
        final Socket socket;
        try {
            socket = new Socket(host, port);
        } catch (IOException E) {
            System.out.println("Cannot connect to server!");
            //E.printStackTrace();
            return;
        };
        final PrintWriter writer = new PrintWriter(new OutputStreamWriter(socket.getOutputStream(), "utf-8"));

        final BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream(), "utf-8"));

        new Main(reader, writer, new Join(botName, botKey));
    }

    final Gson gson = new Gson();
    private PrintWriter writer;
    private ICarAI AI;
    
    public Main(final BufferedReader reader, final PrintWriter writer, final SendMsg join) throws IOException {
//        CVisualizator Visualizator=null;
        this.writer = writer;
        CRace Race=null;
        String line = null;
//        String MyColor="red";
        GameTick=0;
        send(join);
        TrackCoefficient=0.5;
        while((line = reader.readLine()) != null) {
            final MsgWrapper msgFromServer = gson.fromJson(line, MsgWrapper.class);
            if (msgFromServer.msgType.equals("carPositions")) {
                JsonReader Data=new JsonReader(new StringReader(msgFromServer.data.toString()));
                Data.setLenient(true);
                CTickData Tick=new CTickData(Data);
                SendMsg Cmd=AI.ProcessTick(Tick);
//                Visualizator.DisplayCars(Tick,Cmd);
                send(Cmd);
                if (GameTick==0) {
//                    Race.FinishPiece=Tick.MyCar(MyColor).PieceIndex;
                    Race.FinishPiece=0;
                }
                GameTick++;
            } else if (msgFromServer.msgType.equals("join")) {
                System.out.println("Joined");
            } else if (msgFromServer.msgType.equals("gameInit")) {
                System.out.println("Race init");
                System.out.println(msgFromServer.data.toString());
                JsonReader TrackData=new JsonReader(new StringReader(msgFromServer.data.toString()));
                TrackData.setLenient(true);
                Race=new CRace(TrackData);
//                Visualizator=new CVisualizator(Race);
                AI=new PetkaAI(Race);
                GameTick=0;
                Crashed=false;
                TurboAvailable=false;
                Lap=0;
            } else if (msgFromServer.msgType.equals("gameEnd")) {
                System.out.println("Race end");
                return;
            } else if (msgFromServer.msgType.equals("gameStart")) {
                System.out.println("Race start");
            } else if (msgFromServer.msgType.equals("crash")) {
                String CarColor="";
                JsonReader Data=new JsonReader(new StringReader(msgFromServer.data.toString()));
                Data.setLenient(true);
                Data.beginObject();
                while (Data.hasNext()) {
                    String name=Data.nextName();
                    if (name.equals("color")) {
                        CarColor=Data.nextString();
                    } else {
                        Data.skipValue();
                    }
                }
                Data.endObject();
                if (MyColor.equals(CarColor)) {
                    System.out.println("The car has crashed");
                    Crashed=true;
                    TrackCoefficient=TrackCoefficient*0.9;
                }
            } else if (msgFromServer.msgType.equals("spawn")) {
                String CarColor="";
                JsonReader Data=new JsonReader(new StringReader(msgFromServer.data.toString()));
                Data.setLenient(true);
                Data.beginObject();
                while (Data.hasNext()) {
                    String name=Data.nextName();
                    if (name.equals("color")) {
                        CarColor=Data.nextString();
                    } else {
                        Data.skipValue();
                    }
                }
                Data.endObject();
                if (MyColor.equals(CarColor)) {
                    System.out.println("The car has re-spawned");
                    Crashed=false;
                }
            } else if (msgFromServer.msgType.equals("lapFinished")) {
                String CarColor="";
                JsonReader Data=new JsonReader(new StringReader(msgFromServer.data.toString()));
                Data.setLenient(true);
                Data.beginObject();
                while (Data.hasNext()) {
                    String name=Data.nextName();
                    if (name.equals("car")) {
                        Data.beginObject();
                        while (Data.hasNext()) {
                            name=Data.nextName();
                            if (name.equals("color")) {
                                CarColor=Data.nextString();
                            } else {
                                Data.skipValue();
                            }
                        }
                        Data.endObject();
                    } else {
                        Data.skipValue();
                    }
                }
                Data.endObject();
                if (MyColor.equals(CarColor)) {
                    Lap++;
                    System.out.println("Our car has passed one lap ("+Lap+").");
                }
            } else if (msgFromServer.msgType.equals("yourCar")) {
                System.out.println("yourCar message="+msgFromServer.data.toString());
                JsonReader Data=new JsonReader(new StringReader(msgFromServer.data.toString()));
                Data.setLenient(true);
                Data.beginObject();
                while (Data.hasNext()) {
                    String name=Data.nextName();
                    if (name.equals("color")) {
                        MyColor=Data.nextString();
                    } else {
                        Data.skipValue();
                    }
                }
                Data.endObject();
            } else if (msgFromServer.msgType.equals("turboAvailable")) {
                JsonReader Data=new JsonReader(new StringReader(msgFromServer.data.toString()));
                Data.setLenient(true);
                Data.beginObject();
                while (Data.hasNext()) {
                    String name=Data.nextName();
                    if (name.equals("turboDurationMilliseconds")) {
                        TurboDurationMilliseconds=Data.nextDouble();
                    } else if (name.equals("turboDurationTicks")) {
                        TurboDurationTicks=Data.nextDouble();
                    } else if (name.equals("turboFactor")) {
                        TurboFactor=Data.nextDouble();
                    } else {
                        Data.skipValue();
                    }
                }
                Data.endObject();
                if (!Crashed) {
                    TurboAvailable=false;
                    System.out.println("Turbo available! (x="+TurboFactor+",d="+TurboDurationTicks+")");
                }
            } else {
                send(new Ping());
            }
        }
    }

    private void send(final SendMsg msg) {
        writer.println(msg.toJson());
        writer.flush();
    }
}

abstract class SendMsg {
    public String toJson() {
        return new Gson().toJson(new MsgWrapper(this));
    }

    protected Object msgData() {
        return this;
    }

    protected abstract String msgType();
    public abstract String displayString();
}

class MsgWrapper {
    public final String msgType;
    public final Object data;

    MsgWrapper(final String msgType, final Object data) {
        this.msgType = msgType;
        this.data = data;
    }

    public MsgWrapper(final SendMsg sendMsg) {
        this(sendMsg.msgType(), sendMsg.msgData());
    }
}

class CreateRace extends SendMsg {
    class CbotId {
        String name;
        String key;
        public CbotId(final String name,final String key) {
            this.name=name;
            this.key=key;
        }
    }
    public final String trackName;
    public final String password;
    public final CbotId botId;
    CreateRace(final String trackName,final String password,final String botName, final String botKey) {
        this.trackName = trackName;
        this.password = password;
        this.botId=new CbotId(botName,botKey);
    }

    @Override
    protected String msgType() {
        return "createRace";
    }
    
    @Override
    public String displayString() {
        return "Create race";
    }
    
}

class Join extends SendMsg {
    public final String name;
    public final String key;

    Join(final String name, final String key) {
        this.name = name;
        this.key = key;
    }

    @Override
    protected String msgType() {
        return "join";
    }

    @Override
    public String displayString() {
        return "Join";
    }
}

class Ping extends SendMsg {
    @Override
    protected String msgType() {
        return "ping";
    }

    @Override
    public String displayString() {
        return "Ping";
    }
}

class Throttle extends SendMsg {
    private double value;

    public Throttle(double value) {
        this.value = value;
    }

    @Override
    protected Object msgData() {
        return value;
    }

    @Override
    protected String msgType() {
        return "throttle";
    }

    @Override
    public String displayString() {
        return "Throttle : "+value;
    }
}

class SwitchLane extends SendMsg {
    private int direction;

    public SwitchLane(int direction) {
        this.direction = direction;
    }

    @Override
    protected Object msgData() {
        if (direction==-1) return "Left"; else return "Right";
    }

    @Override
    protected String msgType() {
        return "switchLane";
    }
    
    @Override
    public String displayString() {
        return "Switch lane : "+(direction==-1?"Left":"Right");
    }
}

class Turbo extends SendMsg {
    public Turbo() {
    }

    @Override
    protected Object msgData() {
        return "Make it so!";
    }

    @Override
    protected String msgType() {
        return "turbo";
    }
    
    @Override
    public String displayString() {
        return "TURBO";
    }
}