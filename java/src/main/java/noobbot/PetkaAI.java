package noobbot;

public class PetkaAI implements ICarAI
{
    public int LookAheadDepth=6;
    CRace Race;
    CCalc Calc;
    
    public PetkaAI(CRace Race) {
        this.Race=Race;
        this.Calc=new CCalc(Race);
    }
    
    double Throttle = 1;
    
    CCar MyCarL;
    CCar MyCar;
    
    private CPiece NextPiece() {
        return Race.Pieces.get((MyCar.PieceIndex+1)%Race.Pieces.size());
    }
    
    private CPiece ThisPiece() {
        return Race.Pieces.get(MyCar.PieceIndex);
    }
    private double Velocity() {
        double Velocity;
        if (MyCarL!=null) {
            if (MyCarL.PieceIndex==MyCar.PieceIndex) {
                Velocity=MyCar.PieceDistance-MyCarL.PieceDistance;
            } else {
                Velocity=Calc.LaneLength(MyCarL.PieceIndex,MyCarL.LaneIndex)-MyCarL.PieceDistance;
                Velocity+=MyCar.PieceDistance;
            }
        } else {
            Velocity=0;
        }
        return Velocity;
    }
    double LastAngle = 0;
    boolean Poslalswitch = false;
    private double CalculateLaneLength(int Piece,int Lane) {
        double x=0;
        for( int p=Piece; Race.Pieces.get(p).Switch==false; p++){
            x=x+Calc.LaneLength(p, Lane);
            if(p+1==Race.Pieces.size()){
                p=-1;
            }
        }
        return x;
    }
    
    public double StraightPieces(int Piece,int Lane){
        double x=0;  
        for( int p=Piece; Race.Pieces.get(p).Type==EPieceType.Straight; p++){
            x=x+Calc.LaneLength(p, Lane);
            if(p+1==Race.Pieces.size()){
                p=-1;
            }
        }
        //System.out.println("Staright len = "+x);
        return x;
    } 
    
    public boolean FinishLine(int Piece) {
        if (Main.Lap<Race.Laps-1) return false;
        for (int p=Piece;p!=Race.FinishPiece;p=(p+1)%Race.Pieces.size()) {
            if (Race.Pieces.get(p).Type!=EPieceType.Straight) return false;
        }
        return true;
    }
    
    public SendMsg ProcessTick(CTickData Tick) {
        MyCarL=MyCar;
        MyCar=Tick.MyCar(Main.MyColor);
        SendMsg Cmd;
        
       
        if (ThisPiece().Type == EPieceType.Curve){   
            if ((ThisPiece().Angle>0 && LastAngle>MyCar.Angle) || (ThisPiece().Angle<0 && LastAngle<MyCar.Angle)) {
                Cmd = new Throttle(1);
            } else {
                double MaxVelocity;
                MaxVelocity=Calc.MaxVelocity(ThisPiece(),MyCar.LaneIndex,LookAheadDepth);
                if (Velocity()<MaxVelocity*0.9) {
                    Cmd = new Throttle(1);
                } else {
                    Cmd = new Throttle(0);
                }
            }
        } else {
            int NPieces;
            if (Velocity()<=9.0) {
                NPieces=1;
            } else {
                NPieces=1+(int)Math.ceil(Math.max(0,Velocity()-9)/3.0)*2;
            }
            if (Calc.NextPiece(ThisPiece(),NPieces).Type == EPieceType.Curve){
                double MaxVelocity;
                double AngleCoefficient;
                if (Calc.NextPiece(ThisPiece(),NPieces).Angle>0) {
                    if (MyCar.Angle>0) {
                        AngleCoefficient=1-(MyCar.Angle/60.0);
                    } else {
                        AngleCoefficient=1;
                    }
                } else {
                    if (MyCar.Angle<0) {
                        AngleCoefficient=1+(MyCar.Angle/60.0);
                    } else {
                        AngleCoefficient=1;
                    }
                }
                MaxVelocity=Calc.MaxVelocity(Calc.NextPiece(ThisPiece(),NPieces),MyCar.LaneIndex,LookAheadDepth-1)*AngleCoefficient;
                if(Velocity()>MaxVelocity){
                    Cmd = new Throttle(0);
                }else if(Velocity()<=MaxVelocity && Velocity()>MaxVelocity*0.7){
                    Cmd = new Throttle(0.7);
                }else{
                    Cmd = new Throttle(1);
                } 
            }else{
                Cmd = new Throttle(1);
            } 
            
        } 
        
         
        
        if (NextPiece().Switch==true && Poslalswitch==false){
            double Le;
            double LeA;
            double LeB;
            int LaneInA=MyCar.LaneIndex-1;
            int LaneInB=MyCar.LaneIndex+1;
            Le = CalculateLaneLength(Calc.NextPiece(NextPiece()).Index,MyCar.LaneIndex);
            if(MyCar.LaneIndex==0){
                LeB = CalculateLaneLength(Calc.NextPiece(NextPiece()).Index,LaneInB);
                LeA = 10000000000000.0;
            }else if(MyCar.LaneIndex==Race.Lanes.size()-1){
                LeA = CalculateLaneLength(Calc.NextPiece(NextPiece()).Index,LaneInA);  
                LeB = 10000000000000.0;
            }else {
                LeA = CalculateLaneLength(Calc.NextPiece(NextPiece()).Index,LaneInA);
                LeB = CalculateLaneLength(Calc.NextPiece(NextPiece()).Index,LaneInB);
            }

            if(Le<LeA && Le<LeB){
                if (Race.Qualifier || Math.random()<0.7) {
                    
                } else {
                    if (LeA<LeB) Cmd = new SwitchLane(-1); else Cmd = new SwitchLane(1);
                }
            }else if(LeB<LeA && LeB<Le){
                if (Race.Qualifier || Math.random()<0.7) {
                    Cmd = new SwitchLane(1);
                } else {
                    if (LeA<Le) Cmd = new SwitchLane(-1);
                }
            }else if(LeA<Le && LeA<LeB){
                if (Race.Qualifier || Math.random()<0.7) {
                    Cmd = new SwitchLane(-1);
                } else {
                    if (LeB<Le) Cmd = new SwitchLane(1);
                }
            }else{
                double LaneChoice;
                LaneChoice=Math.random();
                if (Race.Qualifier || LaneChoice<0.5) {
                } else if (LaneChoice<0.75) {
                    Cmd = new SwitchLane(-1);
                } else {
                    Cmd = new SwitchLane(1);
                }
            }
            
            Poslalswitch=true;
        }
        if(ThisPiece().Switch == true){
            Poslalswitch = false;
        }
        
        if (Main.TurboAvailable==true && NextPiece().Type == EPieceType.Straight && StraightPieces(NextPiece().Index,MyCar.LaneIndex)>=500.0){  
            Cmd = new Turbo();
            Main.TurboAvailable=false;
        }
        
        if (FinishLine(NextPiece().Index)==true) {
            Cmd=new Throttle(1);
            if (Main.TurboAvailable==true){
                Cmd = new Turbo();
                Main.TurboAvailable=false;
            }
        }
        
        LastAngle=MyCar.Angle;
        return Cmd;
        
    }

}
