package noobbot;

import com.google.gson.stream.JsonReader;
import java.util.ArrayList;
import java.io.*;
import java.awt.Color;


class CCar {
    String Name;
    String Color;
    Color JColor;
    double Angle;
    int PieceIndex;
    double PieceDistance;
    int LaneIndex;
    int LaneIndexEnd;
    int Lap;
    public CCar() {
        
    }
    public CCar(JsonReader Data) throws IOException {
        Data.beginObject();
        while (Data.hasNext()) {
            String name=Data.nextName();
            if (name.equals("id")) {
                Data.beginObject();
                while (Data.hasNext()) {
                    name=Data.nextName();
                    if (name.equals("name")) {
                        this.Name=Data.nextString();
                    } else if (name.equals("color")) {
                        this.Color=Data.nextString();
                        if (this.Color.equals("red")) {
                            this.JColor=this.JColor.RED;
                        } else {
                            this.JColor=this.JColor.GRAY;
                        }
                    }
                }
                Data.endObject();
            } else if (name.equals("angle")) {
                this.Angle=Data.nextDouble();
            } else if (name.equals("piecePosition")) {
                Data.beginObject();
                while (Data.hasNext()) {
                    name=Data.nextName();
                    if (name.equals("pieceIndex")) {
                        this.PieceIndex=Data.nextInt();
                    } else if (name.equals("inPieceDistance")) {
                        this.PieceDistance=Data.nextDouble();
                    } else if (name.equals("lap")) {
                        this.Lap=Data.nextInt();
                    } else if (name.equals("lane")) {
                        Data.beginObject();
                        while (Data.hasNext()) {
                            name=Data.nextName();
                            if (name.equals("startLaneIndex")) {
                                this.LaneIndex=Data.nextInt();
                            } else if (name.equals("endLaneIndex")) {
                                this.LaneIndexEnd=Data.nextInt();
                            }
                        }
                        Data.endObject();
                    }
                }
                Data.endObject();
            }
        }
        Data.endObject();
    }
}

public class CTickData
{
    String GameID;
    int GameTick;
    ArrayList<CCar> Cars;
    
    public CCar MyCar(String Color) {
        int f;
        for (f=0;f<Cars.size();f++) {
            if (Cars.get(f).Color.equals(Color)) {
                return Cars.get(f);
            }
        }
        return null;
    }
    
    public CTickData(int PieceIndex,double PieceDistance,double Angle,int LaneStart,int LaneEnd) {
        Cars=new ArrayList<CCar>();
        CCar NewCar=new CCar();
        NewCar.PieceIndex=PieceIndex;
        NewCar.PieceDistance=PieceDistance;
        NewCar.Angle=Angle;
        NewCar.LaneIndex=LaneStart;
        NewCar.LaneIndexEnd=LaneEnd;
        NewCar.Name="Test car";
        NewCar.Color="red";
        NewCar.JColor=Color.RED;
        Cars.add(NewCar);
    }
    
    public CTickData(JsonReader Data) throws IOException {
        Cars=new ArrayList<CCar>();
        Data.beginArray();
        while (Data.hasNext()) {
            CCar NewCar;
            NewCar=new CCar(Data);
            Cars.add(NewCar);
        }
        Data.endArray();
/*        while (Data.hasNext()) {
            String name=Data.nextName();
            if (name.equals("gameId")) {
                this.GameID=Data.nextString();
            } else if (name.equals("gameTick")) {
                this.GameTick=Data.nextInt();
            }
        }*/
    }
}
